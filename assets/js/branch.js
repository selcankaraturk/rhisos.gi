const bgImg = document.getElementById("bg-img");
const mainTitle = document.getElementById("main-title");
const mainBody = document.getElementById("main-pbody");
const swRubuPlus = document
  .getElementById("rubu-plus")
  .addEventListener("click", RubuPlus);

const swRhisosCyber = document
  .getElementById("cyber-rhisos")
  .addEventListener("click", RhisosCyber);
const swNodi = document
  .getElementById("nodi-cloud")
  .addEventListener("click", Nodi);
const swRhisos = document.querySelectorAll("#rhisos").forEach((item) => {
  item.addEventListener("click", Rhisos);
});

function Nodi() {
  bgImg.classList = "";
  bgImg.classList = "background-nodi";
  mainTitle.innerHTML = "Nodi Cloud";
  mainBody.innerHTML = `Nodi Cloud, kurumların dijital dönüşümüne destek olmak adına geleneksel bulut 
hizmetini, hazırladığı yeni nesil modellerle değiştiriyor ve sektöre taze bir bakış 
açısı kazandırıyor. Tüm bilgileri kendi sunucularında barındırarak hazırladığı bulut 
sistem uygulaması ile katma değerli servis hizmeti sunan Nodi Cloud yenilikçi 
bakış açısı ile alanında öne çıkıyor. <br><br>
IaaS (Infrastructer as a Service - Hizmet Olarak Altyapı) modelinden aldığı ilhamla
tasarladığı, kullanıcı deneyimini temele alarak hazırlanan ara yüzlerle; kobilerin 
ve kobi üstü firmaların ihtiyaçlarını, yönetim paneli üzerinden kendileri için 
gerekli olan özellikleri belirleyerek kuruma en uygun sunucu ve güvenlik 
hizmetlerini de kolayca alabilme imkanı sağlıyor. Ayrıca firma ve kurumların talep
etmesi halinde, tercih edilen tüm servislerin MSP (Manage Service Provider) 
çerçevesinde Nodi Cloud’un alanında yetkin sistem mühendisleri tarafından 
yönetilmesi fırsatı da sunuluyor.`;
  document.title = "Rhisos | Nodi Cloud";

  window.scrollTo({ top: 0, behavior: "smooth" });
}
function RubuPlus() {
  bgImg.classList = "";
  bgImg.classList = "background-rubu";
  mainTitle.innerHTML = "RUBUPLUS";
  mainBody.innerHTML = ` Rhisos’un kurucu ortak olarak yer aldığı, dijital ekran yönetimi uygulaması olan 
RubuPlus, Türk mühendisler tarafından yerli sermaye ve konusunda uzman 
mühendislerin yürüttüğü titiz bir Ar-Ge çalışmasının sonucunda ortaya çıktı. 
Digital signage alanında yeni nesil çalışmalar yapan marka, Türkiye’de hem kamu
hem de özel sektörde birçok firma ile iş ortaklığı içinde bulunmakta, yarattığı 
çözümlerle de birinci sınıf bir hizmet sunmaktadır. <br><br>
RubuPlus ayrıca %100 yerli teknoloji ile geliştirdiği çözümleri yurtdışı pazarına 
ihraç ederek, Türkiye’nin teknoloji ihracatına katkı sağlamaktadır. <br><br>
RubuPlus, var olan içeriklerin paylaşım saati, ekranlarda hangi içeriklerin 
paylaşılacağı, ekran seçimi gibi görsel içerik yönetiminin yanı sıra müşterilerinin 
isteklerine göre birçok farklı ebatta videowall ekranlar da üretmektedir. Ayrıca 
zamanla sektörde ortaya çıkan daha niş ihtiyaçlar için de özel olarak üretimi 
yapılan 10.1 ve 7 inch ekranlarla, firmaların ihtiyacı olan tüm çözümleri tek bir 
çatı altında toplamaktadır.`;
  document.title = "Rhisos | RUBUPLUS";
  window.scrollTo({ top: 0, behavior: "smooth" });
}
function Rhisos() {
  bgImg.classList = "";
  bgImg.classList = "background-rhisos";
  mainTitle.innerHTML = "Rhisos";
  mainBody.innerHTML = ` Rhisos, yerli ve milli teknolojiler üretmenin yanında bu alandaki girişimlere de
                    yatırımlar yapan güçlü bir oluşumu hedeflemektedir. Yerli, millî yenilikçi
                    teknolojilerin ve yazılımların ortaya çıkmasına ve geliştirilmesine katkı sağlayan
                    Rhisos, yaptığı bu yatırımlar ile hem dijital alanda yaşanacak ilerlemenin önü
                    açarken hem de azalan ithalat ve artan ihracat ile ülkemiz ekonomisine ve
                    firmalarımıza büyük katkılar sağlamaktadır. <br><br>
                    Alandaki yirmi yıllık deneyimin üzerine inşa edilen, kurum ve işletmelerin
                    çalışmalarını dijital çağa uygun olarak dönüştürmelerine, yenilemelerine olanak
                    veren, yeni nesil bir teknoloji şirketidir.`;
  document.title = "Rhiso`1s | Anasayfa";
  window.scrollTo({ top: 0, behavior: "smooth" });
}
function RhisosCyber() {
  bgImg.classList = "";
  bgImg.classList = "background-cyber";
  mainTitle.innerHTML = "Cyber Rhisos";
  mainBody.innerHTML = `Teknolojinin ilerlemesiyle birlikte dijital dünyada her gün yaşanan siber güvenlik 
sorunları ve ortaya çıkan güvenlik açıkları, firmaların dijital donanım/yazılım 
sistemleri için yeni bir dijital bir tehlike halini almıştır.  Sürecin bu şekilde devam 
etmesi ise kobi ve kobi üstü şirketlerin siber güvenlik gereksinimlerini arttırmış ve
onlara sıkı güvenlik tedbirleri alma ihtiyacı doğurmuştur. <br><br>
Bu ihtiyaçtan yola çıkan Rhcyber ekibi öncelikle yerli ve milli siber güvenlik 
çözümleriyle ve bununla birlikte Dünya üzerinde güvenlik hizmetleri üreten 
firmaların yazılımlarını hassasiyetle ve büyük bir titizlikle inceler ve elde ettiği 
tüm deneyim ve bilgi birikimi ile şirketlerin ihtiyacı olan siber güvenlik ürünlerini 
detaylıca analiz ederek problemlere en doğru çözüm önerilerini müşterilerine 
sunmaktadır.`;
  document.title = "Rhisos | Cyber Rhisos";
  window.scrollTo({ top: 0, behavior: "smooth" });
}
/*${mainTitle.innerHTML}*/