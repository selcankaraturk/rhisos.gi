const controls = document.querySelector(".controls");
const container = document.querySelector(".thumbnail-container");
const cards = document.querySelectorAll(".item");
const nextButton = document.getElementById("next-button");
const prevButton = document.getElementById("prev-button");
const allBox = container.children;
const containerWidth = container.offsetWidth;
const margin = 30;
var items = 0;
var totalItems = 0;
var jumpSlideWidth = 0;
const slides = [];

// item setup per slide

responsive = [
  { breakPoint: { width: 0, item: 2 } }, //if width greater than 0 (1 item will show)
  { breakPoint: { width: 600, item: 2 } }, //if width greater than 600 (2  item will show)
  { breakPoint: { width: 1000, item: 3 } }, //if width greater than 1000 (4 item will show)
];

function load() {
  for (let i = 0; i < responsive.length; i++) {
    if (window.innerWidth > responsive[i].breakPoint.width) {
      items = responsive[i].breakPoint.item;
    }
  }

  // if (window.innerWidth<1079) {
  //   controls.style.overflow="auto";

  // }

  start();
}

function start() {
  var totalItemsWidth = 0;
  for (let i = 0; i < allBox.length; i++) {
    // if (items === 1) {
    //   allBox[i].style.width = containerWidth / items - margin + "px";
    //   allBox[i].style.height = allBox[i].style.width;
    //   allBox[i].style.margin = margin / 3 + "px";
    //   totalItemsWidth += containerWidth / items;
    //   totalItems++;
    // } else {
    allBox[i].style.width = containerWidth / items - margin + "px";
    allBox[i].style.height = allBox[i].style.width;
    allBox[i].style.margin = margin / 3 + "px";
    totalItemsWidth += containerWidth / items;
    totalItems++;
    // }
  }

  // thumbnail-container width set up
  container.style.width = totalItemsWidth + "px";

  // console.log(nextButton);
  // console.log(prevButton);

  const allPage = Math.ceil(totalItems / items);

  for (let i = 1; i <= allPage; i++) {
    slides[i] = "slide";
    if (i == 1) {
      slides[i] = "active-slide";
    }
  }
  nextButton.setAttribute("onclick", "slideControl('next')");
  prevButton.setAttribute("onclick", "slideControl('prev')");

  cards.forEach((card) => {
    card.addEventListener("click", activedItem);
  });
  slides.shift();

  // slides controls number set up
  // const allSlides = Math.ceil(totalItems / items);
  // const ul = document.createElement("ul");
  // for (let i = 1; i <= allSlides; i++) {
  //   const li = document.createElement("li");
  //   li.id = i;
  //   li.innerHTML = i;
  //   li.setAttribute("onclick", "controlSlides(this)");
  //   ul.appendChild(li);
  //   if (i == 1) {
  //     li.className = "active";
  //   }
  // }

  // controls.appendChild(ul);
}

// // when click on numbers slide to next slide
// function controlSlides(ele) {
//   console.log(ele);
//   // select controls children  'ul' element
//   const ul = controls.children;

//   // select ul children 'li' elements;
//   const li = ul[0].children;

//   var active;

//   for (let i = 0; i < li.length; i++) {
//     if (li[i].className == "active") {
//       // find who is now active
//       active = i;
//       // remove active class from all 'li' elements
//       li[i].className = "";
//     }
//   }
//   // add active class to current slide
//   ele.className = "active";
//   debugger;
//   var numb = ele.id - 1 - active;
//   jumpSlideWidth = jumpSlideWidth + containerWidth * numb;
//   container.style.marginLeft = -jumpSlideWidth + "px";
// }
currentSlide = 1;
function slideControl(route) {
  if (route == "next") {
    k = 1;
  } else {
    k = -1;
  }
  let active;
  for (let i = 0; i < slides.length; i++) {
    if (slides[i] == "active-slide") {
      active = i;
    }
  }

  maxSlide = slides.length;
  minSlide = 1;

  if (minSlide < currentSlide && k == -1) {
    jumpSlideWidth = jumpSlideWidth + (containerWidth / items) * k;
    container.style.marginLeft = -jumpSlideWidth + "px";
    currentSlide += k;
  }
  if (maxSlide > currentSlide && k == 1) {
    jumpSlideWidth = jumpSlideWidth + (containerWidth / items) * k;
    container.style.marginLeft = -jumpSlideWidth + "px";
    currentSlide += k;
  }
}

function activedItem(e) {
  if (!e.target.classList.contains("item")) {
    e = e.target.parentElement;
  } else {
    e = e.target;
  }

  for (let i = 0; i < totalItems; i++) {
    container.children.item(i).classList.remove("selected");
  }

  e.classList.add("selected");
}

window.onload = load();
