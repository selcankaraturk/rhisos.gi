<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=0.85"/>
    <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
            crossorigin="anonymous"
    />
    <link rel="stylesheet" href="assets/css/style.css"/>
    <!-- <link rel="stylesheet" href="./assets/css/style.css" /> -->


    <!-- <script>
      // Initialize and add the map
      function initMap() {
        // The location of Uluru

        const rize = { lat: 41.02927948109252, lng: 40.506979092163384 };
        // The map, centered at Uluru
        const map = new google.maps.Map(document.getElementById("map"), {
          zoom: 15,
          center: rize,
        });
        // The marker, positioned at Uluru
        const marker = new google.maps.Marker({
          position: rize,
          map: map,
        });
      }
    </script> -->

    <title>Rhisos | İletişim</title>
</head>
<body class="background-about-us">


<div class="container-sm">
    <!-- NAVBAR -->
    <div class="row my-3 px-4 header">
        <div class="col-sm-4">
            <a
                    href="index.html"
                    class="logo navbar-brand col-md-4"
                    aria-label="logo"
                    title="logo"
            >
                <img src="assets/icon/logo.svg" alt="logo" class="logo"
                /></a>
        </div>
        <div class="col-md-8 d-flex">
            <a class="nav-link nav-item ms-auto" aria-current="page" href="index.html"
            >Anasayfa</a
            >

            <a class="nav-link" aria-current="page" href="about-us.html">Hakkımızda</a>

            <a class="nav-link active" aria-current="page" href="contact.php">İletişim</a>
        </div>
    </div>

    <!-- NAVBAR -->

    <!-- MAİN -->
    <div class="main">
        <div class="row mt-2">
            <div class="col-lg-6 px-3">
                <div class="header-bold order-1">İletişim</div>
                <div style="margin-top: 32px; margin-left: -6px">
                    <div class="row my-3" style="color:#C4C4C4; font-weight: 500;">
                        <div class="col-1 pe-0">
                            <i class="far fa-building"></i>
                        </div>
                        <div class="col-10 ps-0">
                            <a href="https://www.google.com/maps/d/u/0/edit?mid=1l1uWyUozUxQqrcwiAVaWArfCUorGk8FI&usp=sharing"
                               class="p-footer">
                                Rhisos Teknoloji Bilişim ve Tic. Ltd. Şti.
                            </a>
                        </div>
                    </div>
                    <div class="row my-3" style="color:#C4C4C4; font-weight: 500;">
                        <div class="col-1 pe-0">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                        <div class="col-10 ps-0">
                            <a href="https://www.google.com/maps/d/u/0/edit?mid=1l1uWyUozUxQqrcwiAVaWArfCUorGk8FI&usp=sharing"
                               class="p-footer">
                                Levent Mah. Cömert Sk. No:1 Yapı Kredi Plaza C Block <br> Kat:17 No:40-41, Beşiktaş
                                İstanbul
                            </a>
                        </div>
                    </div>
                    <div class="row my-3" style="color:#C4C4C4;  font-weight: 500;">
                        <div class="col-1 pe-0">
                            <i class="fas fa-phone-alt"></i>
                        </div>
                        <div class="col-10 ps-0">
                            <a href="tel:+90 539 571 80 90" class="p-footer">
                                0 212 317 47 37
                            </a>
                        </div>
                    </div>

                    <div class="row my-3" style="color:#C4C4C4;  font-weight: 500;">
                        <div class="col-1 pe-0">
                            <i class="far fa-envelope"></i>
                        </div>
                        <div class="col-10 ps-0">
                            <a href="mailto:info@rhisos.com" class="p-footer">
                                info@rhisos.com
                            </a>
                        </div>
                    </div>

                    <div style="margin-top: 20px; margin-left: 0px">
                        <div class="fixed mb-3">
                            <a href="https://twitter.com/RhisosTech" class="social-media-item pe-1"
                            ><img src="assets/icon/twitter.svg" alt="" style="height: 22px"
                                /></a>
                            <a href="" class="social-media-item px-1"
                            ><img src="assets/icon/facebook.svg" alt="" style="height: 22px"
                                /></a>
                            <a href="https://www.instagram.com/rhisostech/" class="social-media-item px-1"
                            ><img src="assets/icon/instagram.svg" alt="" style="height: 22px"
                                /></a>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div id="map" class="ps-0">
                        <!--<a href="https://www.google.com/maps/d/u/0/edit?mid=1l1uWyUozUxQqrcwiAVaWArfCUorGk8FI&usp=sharing">
                          <img src="assets/img/map.JPG" class="img-fluid" alt="">
                        </a>-->
                        <iframe src="https://www.google.com/maps/d/u/0/embed?mid=1l1uWyUozUxQqrcwiAVaWArfCUorGk8FI&ehbc=2E312F"></iframe>
                    </div>
                </div>
                <div>

                </div>
            </div>


            <div class="col-lg-6 px-3 pb-3 order-2">
                <center>

                    <h4 class="contact-main-header mb-3 ">Detaylı Bilgi Almak İçin Bize Ulaşabilirsiniz. </h4>
                </center>
                <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
                    <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
                    </symbol>
                    <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
                        <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
                    </symbol>
                    <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
                        <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                    </symbol>
                </svg>
                <?php if (isset($_GET['success'])): ?>

                <div class="alert alert-success d-flex align-items-center alert-dismissible fade show" role="alert"
                ">
                <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:">
                    <use xlink:href="#check-circle-fill"/>
                </svg>
                <div>Mesajınız Başarıyla İletildi</div>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            <?php endif ?>
            <?php if (isset($_GET['error'])): ?>

            <div class="alert alert-danger d-flex align-items-center alert-dismissible fade show" role="alert"
            ">
            <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
            <div>Üzgünüz Mesajınız İletilemedi</div>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        <?php endif ?>

        <form action="mail.php" method="POST">
            <div class="form-floating mb-3">
                <input type="text" class="form-control" required name="name" id="floatingInput" placeholder="Ad Soyad">
                <label for="floatingInput" class="form-control-placeholder">İsim Soyisim</label>
            </div>
            <div class="form-floating mb-3">
                <input type="email" class="form-control" required name="email" id="floatingPassword" placeholder="Mail">
                <label for="floatingInput" class="form-control-placeholder">Mail adresiniz</label>
            </div>
            <div class="form-floating">
                        <textarea class="form-control" required placeholder="Leave a comment here" name="message"
                                  id="floatingTextarea2" style="height: 300px"></textarea>
                <label for="floatingTextarea2" class="form-control-placeholder">Mesajınız</label>
            </div>
            <button type="submit" class="btn btn-danger mt-3 form-button" style="width: 150px;"> Gönder</button>
            <input type="hidden" name="action" value="send">
        </form>
    </div>
</div>
</div>
</div>

</div>
<script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"
></script>
<script
        src="https://kit.fontawesome.com/c8cb978e88.js"
        crossorigin="anonymous"
></script>

<script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDVZ5jPvTQhTDCOZtiYmNabMa0R1Opr_vw"
        async
></script>
<!--    <script src="assets/js/google-maps.js"></script>-->


</body>
</html>
